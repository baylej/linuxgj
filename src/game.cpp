/*
	Copyright 2017 Jonathan Bayle

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include <game.hpp>

#include <iostream>

using namespace std::literals;

using vivace::Vivace;
using vivace::Vivace_Error;

int main(int argc, char* argv[])
{
	try {
		Vivace vivace("Linux Game Jam Entry"s, "Mr-Hide"s);
	}
	catch (Vivace_Error& error) {
		std::cerr << error.what() << std::endl;
	}
}
